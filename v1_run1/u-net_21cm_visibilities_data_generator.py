#!/usr/bin/env python
# coding: utf-8

import os
import glob

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math

import tensorflow as tf
from tensorflow import keras

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

import graphviz
import pydot

from pyuvdata import UVData
from pyuvdata.data import DATA_PATH

from tensorflow.keras.utils import plot_model

from sklearn.metrics import roc_curve, auc

tf.test.gpu_device_name()

tf.test.is_gpu_available(cuda_only=False, min_cuda_compute_capability=None)

#def build_unet(activation='relu', input_shape=(None, None, 1)):
def build_unet(activation='relu', input_shape=(56, 768, 1)):
    INPUT = keras.layers.Input(shape=input_shape)

    # Downsampling
    downsampling_block1 = keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation=activation)(INPUT)
    downsampling_block1 = keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation=activation)(downsampling_block1)
    downsampling_block1 = keras.layers.MaxPooling2D()(downsampling_block1)

    #print("downsampling_block1: ", downsampling_block1.shape)
    
    downsampling_block2 = keras.layers.Conv2D(filters=128, kernel_size=3, padding='same', activation=activation)(downsampling_block1)
    downsampling_block2 = keras.layers.Conv2D(filters=128, kernel_size=3, padding='same', activation=activation)(downsampling_block2)
    downsampling_block2 = keras.layers.MaxPooling2D()(downsampling_block2)

    ##print("downsampling_block2: ", downsampling_block2.shape)

    downsampling_block3 = keras.layers.Conv2D(filters=256, kernel_size=3, padding='same', activation=activation)(downsampling_block2)
    downsampling_block3 = keras.layers.Conv2D(filters=256, kernel_size=3, padding='same', activation=activation)(downsampling_block3)
    downsampling_block3 = keras.layers.MaxPooling2D()(downsampling_block3)

    #print("downsampling_block3: ", downsampling_block3.shape)

    downsampling_block4 = keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation=activation)(downsampling_block3)
    downsampling_block4 = keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation=activation)(downsampling_block4)
    downsampling_block4 = keras.layers.MaxPooling2D()(downsampling_block4)
    #downsampling_block4 = keras.layers.MaxPooling2D(strides=3)(downsampling_block4)

    #print("downsampling_block4: ", downsampling_block4.shape)

    downsampling_block5 = keras.layers.Conv2D(filters=1024, kernel_size=3, padding='same', activation=activation)(downsampling_block4)
    downsampling_block5 = keras.layers.Conv2D(filters=1024, kernel_size=3, padding='same', activation=activation)(downsampling_block5)
    downsampling_block5 = keras.layers.MaxPooling2D()(downsampling_block5)

    #print("downsampling_block5: ", downsampling_block5.shape)
    
    # Upsampling
    upsampling_block1 = keras.layers.Conv2DTranspose(filters=1024, kernel_size=2, strides=2, padding='same')(downsampling_block5)
    upsampling_block1 = keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation=activation)(upsampling_block1)
    
    #print("downsampling_block4: ", downsampling_block4.shape, "upsampling_block1: ", upsampling_block1.shape)

    crop_downsampling_block4 = keras.layers.Cropping2D(cropping=((1, 0), (0, 0)))(downsampling_block4)
    #SHORTCUT4 = keras.layers.Concatenate(name='SHORTCUT4')([downsampling_block4, upsampling_block1])
    #print("crope_downsampling_block4: ", crop_downsampling_block4.shape, "upsampling_block1: ", upsampling_block1.shape)
    SHORTCUT4 = keras.layers.Concatenate(name='SHORTCUT4')([crop_downsampling_block4, upsampling_block1])
    
    upsampling_block2 = keras.layers.Conv2DTranspose(filters=512, kernel_size=2, strides=2, padding='same')(SHORTCUT4)
    upsampling_block2 = keras.layers.Conv2D(filters=256, kernel_size=3, padding='same', activation=activation)(upsampling_block2)

    #print("upsampling_block2: ", upsampling_block2.shape)
    #print("SHAPES BEFORE SHORTCUT3!!!!!!!!!!!!!!!!!!!!!!!!!: ", downsampling_block3.shape, upsampling_block2.shape)
    crop_downsampling_block3 = keras.layers.Cropping2D(cropping=((3, 0), (0, 0)))(downsampling_block3)
    #print("crop_downsampling_block3: ", crop_downsampling_block3.shape, "upsampling_block2: ", upsampling_block2.shape)
    
    #SHORTCUT3 = keras.layers.Concatenate(name='SHORTCUT3')([downsampling_block3, upsampling_block2])
    SHORTCUT3 = keras.layers.Concatenate(name='SHORTCUT3')([crop_downsampling_block3, upsampling_block2])
    
    upsampling_block3 = keras.layers.Conv2DTranspose(filters=256, kernel_size=2, strides=2, padding='same')(SHORTCUT3)
    upsampling_block3 = keras.layers.Conv2D(filters=128, kernel_size=3, padding='same', activation=activation)(upsampling_block3)

    crop_downsampling_block2 = keras.layers.Cropping2D(cropping=((6, 0), (0, 0)))(downsampling_block2)
    #print("crop_downsampling_block2: ", crop_downsampling_block2.shape, "upsampling_block3: ", upsampling_block3.shape)
    #SHORTCUT2 = keras.layers.Concatenate(name='SHORTCUT2')([downsampling_block2, upsampling_block3])
    SHORTCUT2 = keras.layers.Concatenate(name='SHORTCUT2')([crop_downsampling_block2, upsampling_block3])
    
    upsampling_block4 = keras.layers.Conv2DTranspose(filters=128, kernel_size=2, strides=2, padding='same')(SHORTCUT2)
    upsampling_block4 = keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation=activation)(upsampling_block4)

    crop_downsampling_block1 = keras.layers.Cropping2D(cropping=((12, 0), (0, 0)))(downsampling_block1)
    #print("crop_downsampling_block1: ", crop_downsampling_block1.shape, "upsampling_block4: ", upsampling_block4.shape)
    #SHORTCUT1 = keras.layers.Concatenate(name='SHORTCUT1')([downsampling_block1, upsampling_block4])
    SHORTCUT1 = keras.layers.Concatenate(name='SHORTCUT1')([crop_downsampling_block1, upsampling_block4])

    # Output
    OUTPUT = keras.layers.Conv2D(filters=1, kernel_size=1, padding='same', activation='sigmoid')(SHORTCUT1)

    #print("OUTPUT SHAPE: ", OUTPUT.shape)
    
    # Build and compile model
    model = keras.Model(inputs = INPUT, outputs = OUTPUT)
    #model = keras.Model(inputs = INPUT, outputs = SHORTCUT4)

    return model


# build
unet_model = build_unet()

optimizer = keras.optimizers.SGD(learning_rate=1e-3)
unet_model.compile(loss=tf.keras.losses.BinaryCrossentropy(), optimizer=optimizer, metrics=[tf.keras.metrics.AUC()])

unet_model.summary()

keras.utils.plot_model(unet_model, "/users/sdubey11/results/21cm/unet/u_net_21cm_visibilities.png", show_shapes=True)

class CustomDataGenerator(tf.keras.utils.Sequence):

    def __init__(self, df, batch_size, input_size=(None, 1), reshape=True, shuffle=True):
        self.df = df.copy()
        self.batch_size = batch_size
        self.input_size = input_size
        self.shuffle = shuffle
        self.n = len(self.df)
        self.current_file = 0
        self.current_start_index = 0
        self.reshape = reshape

    def center_crop(self, img, output_size):
        h, w = img.shape[0], img.shape[1]
        target_h, target_w = output_size

        top = (h - target_h) // 2
        left = (w - target_w) // 2
        bottom = top + target_h
        right = left + target_w

        return img[top:bottom, left:right]

    def reshape_image(self, image_np_array, num_maxpool):
        # Convert numpy array to TensorFlow tensor
        image = tf.convert_to_tensor(image_np_array)

        # Add a channel dimension if missing (for grayscale images)
        if image.shape.ndims == 2:
            image = tf.expand_dims(image, axis=-1)  # Add a channel dimension
        elif image.shape.ndims != 3:
            raise ValueError("Expected image to have 2 or 3 dimensions, but got {} dimensions".format(image.shape.ndims))


        # Get original dimensions
        shape = tf.shape(image)
        #print("SHAPE: ", shape)
        height, width = shape[0], shape[1]

        # Calculate the target dimensions
        target_height = tf.cast(tf.math.ceil(height / 2**num_maxpool) * 2**num_maxpool, tf.int32)
        target_width = tf.cast(tf.math.ceil(width / 2**num_maxpool) * 2**num_maxpool, tf.int32)

        # Resize the image
        image = tf.image.resize(image, [target_height, target_width])

        return image

    def preprocess_numpy_image(self, tensor):
        n_maxpool = 5
        pulse = self.reshape_image(image_np_array=tensor, num_maxpool=n_maxpool)
        return tensor

    def __get_data(self, FILE, start_index):

        column = ["filenames"]

        # Initialize lists to hold data and labels
        waterfall_list = []
        waterfall_pixel_flags = []

        # open file
        uvd = UVData.from_file(FILE, use_future_array_shapes=True)

        # Get all baselines and all polarizations
        antpairpols = uvd.get_antpairpols()

        waterfall_list = []
        waterfall_pixel_flags = []
             
        next_index = start_index
   
        # Get data (visibilities)
        #for antpairpol in antpairpols:
        for current_index in range(start_index, len(antpairpols)):
            antpairpol = antpairpols[current_index]
            data = np.abs(uvd.get_data(antpairpol))
            waterfall_list.append(data)
            
            # Get flags from AOFlagger
            waterfall_pixel_flags.append(uvd.get_flags(antpairpol))

            if len(waterfall_list) >= self.batch_size:
                next_index = current_index + 1
                break

        # Check if no data has been added to waterfall_list
        if not waterfall_list:
            next_index = -1
        
        # Create NumPy arrays; TensorFlow/Keras like this
        X_batch = np.array(waterfall_list)
        y_batch = np.array(waterfall_pixel_flags)

        if self.reshape:
            data_reshaped = np.array([self.preprocess_numpy_image(waterfall) for waterfall in waterfall_list])
            y_batch = np.array([self.center_crop(label, (16, 384)) for label in y_batch])
            return data_reshaped, y_batch, next_index
        else:
            return X_batch, y_batch, next_index

    def on_epoch_end(self):
        if self.shuffle:
            self.df = self.df.sample(frac=1).reset_index(drop=True)

        self._current_file = 0

    def __getitem__(self, index):
        # Initially set X and y to empty arrays
        X, y = np.empty(0), np.empty(0)
        
        # Loop until we have enough data for a batch
        while len(X) < self.batch_size:
            # If we've read all files, break the loop
            if self.current_file >= len(self.df):
                break

            # Fetch file path
            file_path = self.df.iloc[self.current_file]["filenames"]

            # Fetch data from the current file starting from the current_start_index
            X_batch, y_batch, next_index = self.__get_data(file_path, self.current_start_index)
            
            # Append the fetched data to X and y
            X = np.vstack([X, X_batch]) if X.size else X_batch
            y = np.vstack([y, y_batch]) if y.size else y_batch

            # If next_index is -1, it means we've reached the end of the current file and should move to the next one
            if next_index == -1:
                self.current_file += 1
                self.current_start_index = 0
            else:
                self.current_start_index = next_index

        return X, y

    def __len__(self):
        return self.n // self.batch_size

# Set the data path
DATA_PATH = '/oscar/data/jpober/shared/WL_PhaseII/RAWOBS'


# Get uvfits file names
filenames = glob.glob(os.path.join(DATA_PATH, '*.uvfits'))

df = pd.DataFrame({"filenames": filenames})
df = df.sample(frac=1)

df_temp_train, df_test = train_test_split(df, test_size=0.2, random_state=42)

# split further to get a test set
df_train, df_val = train_test_split(df_temp_train, test_size=0.2, random_state=42)

df_temp_train, df_test = train_test_split(df, test_size=0.2, random_state=42)

# split further to get a test set
df_train, df_val = train_test_split(df_temp_train, test_size=0.2, random_state=42)

traingen = CustomDataGenerator(df_train, batch_size=16)

valgen = CustomDataGenerator(df_val, batch_size=16)

reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.00001)
my_callbacks = [keras.callbacks.EarlyStopping(patience=50), reduce_lr]
#my_callbacks = [keras.callbacks.EarlyStopping(patience=10)]

training_history = unet_model.fit(traingen, validation_data=valgen, epochs=50, verbose=2)
#training_history = unet_model.fit(traingen, validation_data=valgen, epochs=500, callbacks=[my_callbacks], verbose=2)
#training_history = model.fit(x=X_train, y=y_train, batch_size=128, epochs=100, validation_split=0.2, callbacks=[my_callbacks], verbose=2)

unet_model.save("/users/sdubey11/results/21cm/unet/21cm_mwa_unet_AOFLAGGER")

pd.DataFrame(training_history.history).plot(figsize=(20, 15))
plt.xlabel('Epoch', fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.grid(True)
plt.ylim(top=1.2)
plt.ylim(bottom=0)
fname = "/users/sdubey11/results/21cm/unet/unet_training_history_aoflagger.png"
plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', transparent=False, bbox_inches=None, pad_inches=0.1)
plt.show()

def plot_roc_curve_pp(fpr=[], tpr=[], labels=[], auc_tot=[], title=""):
        fname = "/users/sdubey11/results/21cm/unet/roc_auc_21cm_unet_aoflagger.png"
        fig, ax = plt.subplots()
        ax.plot(fpr[0], tpr[0], label='Test '+str(round(auc_tot[0], 3)))
        ax.plot(fpr[1], tpr[1], label='Train '+str(round(auc_tot[1], 3)))
        plt.plot([0, 1], [0, 1], 'k--')
        legend = ax.legend(loc="lower right")
        plt.xlabel("FPR", fontsize=16)
        plt.ylabel("TPR", fontsize=16)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)
        plt.grid()
        plt.title("ROC + AUC for "+title)
        plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', transparent=False, bbox_inches=None, pad_inches=0.1)
        plt.show()


# testgen for ROC AUC
testgen = CustomDataGenerator(df_test, batch_size=16)  # Assuming you have a similar data generator for testing

y_true = []
y_pred = []

# Iterate over the test generator
for _ in range(len(testgen)):
    X_test, Y_test = testgen[_]  # Getting the test batch
    preds = unet_model.predict(X_test)  # Predicting using your trained model

    # Flatten Y_test and preds if necessary
    Y_test = Y_test.flatten()
    preds = preds.flatten()

    # Store the true labels and the predictions
    y_true.extend(Y_test)
    y_pred.extend(preds)

# Calculate the ROC curve points
fpr, tpr, thresholds = roc_curve(y_true, y_pred)
roc_auc = auc(fpr, tpr)

y_true_train = []
y_pred_train = []


# Iterate over the test generator
for _ in range(len(traingen)):
    X_train, Y_train = traingen[_]  # Getting the test batch
    preds = unet_model.predict(X_test)  # Predicting using your trained model
    # Flatten Y_test and preds if necessary
    Y_train = Y_train.flatten()
    preds = preds.flatten()
    # Store the true labels and the predictions
    y_true_train.extend(Y_train)
    y_pred_train.extend(preds)


fpr_train, tpr_train, thresholds = roc_curve(y_true_train, y_pred_train)
roc_auc_train = auc(fpr_train, tpr_train)

fpr_tot = [fpr, fpr_train]
tpr_tot = [tpr, tpr_train]
auc_tot = [roc_auc, roc_auc_train]

labels = ["test", "train"]

plot_roc_curve_pp(fpr_tot, tpr_tot, labels, auc_tot, " ")
